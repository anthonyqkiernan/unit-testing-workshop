﻿using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;

namespace UnitTestingWorkshop.BusinessLogic.Services
{
    public class ForumMemberService
    {

        private readonly IMemberService _memberService;

        public ForumMemberService()
        {
            // Exercise 1 - use contructor injection to inject this dependency
            _memberService = UmbracoContext.Current.Application.Services.MemberService;
        }


        /// <summary>
        /// Create member.
        /// </summary>
        /// <returns></returns>
        public string Create(string name, string email, string plainTextPassword)
        {
            // TODO: if member exists, return "exists"

            var member = _memberService.CreateMember(email, email, name, "Member");
            _memberService.SavePassword(member, plainTextPassword);
            _memberService.Save(member);

            return "success";
        }




        /// <summary>
        /// Gets member.
        /// </summary>
        public umbraco.cms.businesslogic.member.Member GetMemberById(int id)
        {
            var member = new umbraco.cms.businesslogic.member.Member(id);

            return member;
        }




        /// <summary>
        /// Gets member. Stores in request cache.
        /// </summary>
        public IMember GetCurrentMember(string email)
        {
            var cacheKey = "member_" + email;

            var member = HttpContext.Current.Session[cacheKey] as IMember;

            if (member == null)
            {
                member = _memberService.GetByEmail(email);

                // cache member is request so we don't have to query Umbraco again
                HttpContext.Current.Session.Add(cacheKey, member);
            }

            return member;
        }
    }
}
